using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour
{
    public float _health;

    private Transform _player;
    private NavMeshAgent _navMeshAgent;

    private void Start()
    {
        _player = CharacterMovementController.GetTransform();
        _navMeshAgent = GetComponent<NavMeshAgent>();
    }


    private void Update()
    {
        _navMeshAgent.SetDestination(_player.position);
    }

    public void OnTakeDamage(float damage)
    {
        _health -= damage;
        if (_health <= 0)
        {
            GetComponent<RagdollActivator>().ActivateRagdoll();
            GetComponent<NavMeshPathWalker>().enabled = false;
            this.enabled = false;
        }
    }
}
