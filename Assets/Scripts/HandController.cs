using UnityEngine;

public class HandController : MonoBehaviour
{
    [SerializeField] private float _yRotationLimit = 90f;

    void Update()
    {
        var targetDir = MousePosition.GetPosition() - transform.position;
        targetDir.y = 0f;
        
        var targetRotation = Quaternion.LookRotation(targetDir);
        var transformExample = transform;
        
        transformExample.rotation = targetRotation;
        var angle = transformExample.localRotation.eulerAngles.y < 180 ? transformExample.localRotation.eulerAngles.y : 360 - transformExample.localRotation.eulerAngles.y;
        transform.localRotation = Quaternion.Euler(0,Mathf.Clamp(angle,0,_yRotationLimit),0);
    }
}