using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class GameWorld : MonoBehaviour
{

    [SerializeField] private ChunkRenderer _chunkPrefab;
    [SerializeField] private int _viewRadius = 8;
    private Vector2Int _currentPlayerChunk;

    public Dictionary<Vector2Int, CunkData> CunksDatas = new Dictionary<Vector2Int, CunkData>();
    public Transform _player;
    public TerrainGenerator _generator;

    [ContextMenu("Regenerate World")]
    public void Regenerate() 
    {
        _generator.Init();
        foreach(var chunkData in CunksDatas)
        {
            Destroy(chunkData.Value.Renderer.gameObject);
        }
        CunksDatas.Clear();

        
        StartCoroutine(Generate(false));
    }


    private void Start()
    {
        StartCoroutine(Generate(false));
    } 

    private IEnumerator Generate(bool wait)
    {
        for (int x = _currentPlayerChunk.x - _viewRadius; x < _currentPlayerChunk.x + _viewRadius; x++)
        {
            for (int z = _currentPlayerChunk.y - _viewRadius; z < _currentPlayerChunk.y + _viewRadius; z++)
            {
                Vector2Int chunkPosition = new Vector2Int(x, z);
                if (CunksDatas.ContainsKey(chunkPosition)) continue;
                 
                LoadChunkAt(chunkPosition);

                if (wait) yield return new WaitForSecondsRealtime(0.2f);
            }
        }
    }

    private void LoadChunkAt(Vector2Int chunkPosition)
    {
        float xPos = chunkPosition.x * ChunkRenderer.ChunkWight * ChunkRenderer.BlockScale;
        float zPos = chunkPosition.y * ChunkRenderer.ChunkWight * ChunkRenderer.BlockScale;

        var cunkData = new CunkData();
        cunkData.ChunkPosition = chunkPosition;
        //cunkData.Renderer = cunkData.GetComponent<Renderer>();
        cunkData.Blocks = _generator.GenerateTerrain(xPos, zPos);
        CunksDatas.Add(chunkPosition, cunkData);


        var chunk = Instantiate(_chunkPrefab, new Vector3(xPos, 0, zPos), Quaternion.identity, transform);
        chunk.ChunkData = cunkData;
        chunk._paretWorld = this;
    }

    private void Update()
    {
        Vector3Int playerWorldPos = Vector3Int.FloorToInt(_player.position / ChunkRenderer.BlockScale);
        Vector2Int playerChunk = GetChunkContainingBlock(playerWorldPos);
        if(playerChunk != _currentPlayerChunk)
        {
            _currentPlayerChunk = playerChunk;
            StartCoroutine(Generate(true));
        }
    }

    public Vector2Int GetChunkContainingBlock(Vector3Int blockWorldPos)
    {
        Vector2Int chunkPosition = new Vector2Int(blockWorldPos.x / ChunkRenderer.ChunkWight, blockWorldPos.z / ChunkRenderer.ChunkWight);


        if (blockWorldPos.x < 0) chunkPosition.x--;
        if (blockWorldPos.y < 0) chunkPosition.y--;


        return chunkPosition;
    }
}
