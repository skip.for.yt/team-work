using System;
using UnityEngine;

public class BrigeGenerator : MonoBehaviour
{
    public NoiseOctaveSettings _octaves;
    public float _baseHeight = 4;
    public float _brigeStep = 0.2f;
    public float _seed = 2;

    [Serializable]
    public class NoiseOctaveSettings
    {
        public FastNoiseLite.NoiseType _noiseType;
        public float _frequency = 0.01f;
    }

    private FastNoiseLite _octaveNoises;

    public void Init()
    {
        _octaveNoises = new FastNoiseLite();
        _octaveNoises.SetNoiseType(_octaves._noiseType);
        _octaveNoises.SetFrequency(_octaves._frequency);
        
    }
    public bool BlockIsBrige(float x, float y)
    {
        float result = 0;
        float noise = _octaveNoises.GetNoise(x * _seed, y * _seed);
        result += noise;

        if(result > 0 && result < _brigeStep) 
            return true;

        return false;
    }
}
