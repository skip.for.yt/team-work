using System;
using UnityEngine;

public class RiverGenerator : MonoBehaviour
{
    public NoiseOctaveSettings _octaves;
    public float _baseHeight = 4;
    public float riverStep = 0.2f;

    [Serializable]
    public class NoiseOctaveSettings
    {
        public FastNoiseLite.NoiseType _noiseType;
        public float _frequency = 0.01f;
    }

    private FastNoiseLite _octaveNoises;

    public void Init()
    {
        _octaveNoises = new FastNoiseLite();
        _octaveNoises.SetNoiseType(_octaves._noiseType);
        _octaveNoises.SetFrequency(_octaves._frequency);
        
    }
    public bool BlockIsRiver(float x, float y)
    {
        float result = 0;
        float noise = _octaveNoises.GetNoise(x, y);
        result += noise;

        if(result > 0 && result < riverStep) 
            return true;

        return false;
    }
}
