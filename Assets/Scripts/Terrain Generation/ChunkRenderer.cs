using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ChunkRenderer : MonoBehaviour
{

    public const int ChunkWight = 10;
    public const int ChunkHeight = 128;
    public const float BlockScale = 0.5f;


    public CunkData ChunkData;
    [FormerlySerializedAs("ParetWorld")] public GameWorld _paretWorld;

   

    private List<Vector3> _verticies = new List<Vector3>();
    private List<Vector2> _uvs = new List<Vector2>();
    private List<int> _triangles = new List<int>();
    private void Start()
    {
        Mesh chunkMesh = new Mesh();
        ChunkData.Renderer = GetComponent<Renderer>();

        for (int y = 0; y < ChunkHeight; y++)
        {
            for(int x = 0; x < ChunkWight; x++)
            {
                for(int z = 0; z < ChunkWight; z++)
                {
                    GenerateBlock(x, y, z);
                }
            }
        }
        
        
        chunkMesh.vertices = _verticies.ToArray();
        chunkMesh.triangles = _triangles.ToArray();
        chunkMesh.uv = _uvs.ToArray();  


        chunkMesh.RecalculateNormals(); 
        chunkMesh.RecalculateBounds();


        chunkMesh.Optimize();


        GetComponent<MeshFilter>().mesh = chunkMesh;
        GetComponent<MeshCollider>().sharedMesh = chunkMesh;
    }



    private BlockType GetBlockAtPosition(Vector3Int blockPosition)
    {
        if(blockPosition.x >= 0 && blockPosition.x < ChunkWight &&
            blockPosition.y >= 0 && blockPosition.y < ChunkHeight &&
            blockPosition.z >=0 && blockPosition.z < ChunkWight)
            return ChunkData.Blocks[blockPosition.x, blockPosition.y, blockPosition.z];

        if (blockPosition.y < 0 || blockPosition.y > ChunkWight) return BlockType.Air;

        Vector2Int adjacentChunkPosition = ChunkData.ChunkPosition;
        if (blockPosition.x < 0)
        {
            adjacentChunkPosition.x--;
            blockPosition.x += ChunkWight;
        }
        else if (blockPosition.x >= ChunkWight)
        {
            adjacentChunkPosition.x++;
            blockPosition.x -= ChunkWight;
        }

        if (blockPosition.z < 0)
        {
            adjacentChunkPosition.y--;
            blockPosition.z += ChunkWight;
        }
        else if (blockPosition.z >= ChunkWight)
        {
            adjacentChunkPosition.y++;
            blockPosition.z -= ChunkWight;
        }


        if(_paretWorld.CunksDatas.TryGetValue(adjacentChunkPosition, out CunkData adjacentChunk))
        {
            return adjacentChunk.Blocks[blockPosition.x,blockPosition.y,blockPosition.z];
        }
        

        return BlockType.Air;
       
    }

    private void GenerateBlock(int x, int y, int z)
    {
        var blockPosition = new Vector3Int(x, y, z);

        if (GetBlockAtPosition(blockPosition) == 0) return;

        if (GetBlockAtPosition(blockPosition + Vector3Int.right) == 0)
        {
            GenerateRightSite(blockPosition);
            AddUVs(GetBlockAtPosition(blockPosition));
        }

        if (GetBlockAtPosition(blockPosition + Vector3Int.left) == 0)
        {
            GenerateLeftSite(blockPosition);
            AddUVs(GetBlockAtPosition(blockPosition));
        }

        if (GetBlockAtPosition(blockPosition + Vector3Int.forward) == 0)
        {
            GenerateFrontSite(blockPosition);
            AddUVs(GetBlockAtPosition(blockPosition));
        }

        if (GetBlockAtPosition(blockPosition + Vector3Int.back) == 0)
        {
            GenerateBackSite(blockPosition);
            AddUVs(GetBlockAtPosition(blockPosition));
        }

        //if (GetBlockAtPosition(blockPosition + Vector3Int.down) == 0) GenerateBottopSite(blockPosition);

        if (GetBlockAtPosition(blockPosition + Vector3Int.up) == 0)
        {
            GenerateTopSite(blockPosition);
            AddUVs(GetBlockAtPosition(blockPosition));
        }
    }

    private void GenerateRightSite(Vector3Int blockPosition)
    {
        _verticies.Add(BlockScale *(new Vector3(1, 0, 0) + blockPosition));
        _verticies.Add(BlockScale *(new Vector3(1, 1, 0) + blockPosition));
        _verticies.Add(BlockScale *(new Vector3(1, 0, 1) + blockPosition));
        _verticies.Add(BlockScale *(new Vector3(1, 1, 1) + blockPosition));

        AddLastVerticiesSquare();
    }

    private void GenerateLeftSite(Vector3Int blockPosition)
    {
        _verticies.Add(BlockScale * (new Vector3(0, 0, 0) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(0, 0, 1) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(0, 1, 0) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(0, 1, 1) + blockPosition));

        AddLastVerticiesSquare();
    }

    private void GenerateFrontSite(Vector3Int blockPosition)
    {
        _verticies.Add(BlockScale * (new Vector3(0, 0, 1) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(1, 0, 1) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(0, 1, 1) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(1, 1, 1) + blockPosition));

        AddLastVerticiesSquare();
    }

    private void GenerateBackSite(Vector3Int blockPosition)
    {
        _verticies.Add(BlockScale * (new Vector3(0, 0, 0) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(0, 1, 0) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(1, 0, 0) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(1, 1, 0) + blockPosition));

        AddLastVerticiesSquare();
    }

    private void GenerateBottopSite(Vector3Int blockPosition)
    {
        _verticies.Add(BlockScale * (new Vector3(0, 0, 0) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(1, 0, 0) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(0, 0, 1) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(1, 0, 1) + blockPosition));

        AddLastVerticiesSquare();
    }

    private void GenerateTopSite(Vector3Int blockPosition)
    {
        _verticies.Add(BlockScale * (new Vector3(0, 1, 0) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(0, 1, 1) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(1, 1, 0) + blockPosition));
        _verticies.Add(BlockScale * (new Vector3(1, 1, 1) + blockPosition));

        AddLastVerticiesSquare();
    }

    private void AddLastVerticiesSquare()
    {
        _triangles.Add(_verticies.Count - 4);
        _triangles.Add(_verticies.Count - 3);
        _triangles.Add(_verticies.Count - 2);

        _triangles.Add(_verticies.Count - 3);
        _triangles.Add(_verticies.Count - 1);
        _triangles.Add(_verticies.Count - 2);
    }

    private void AddUVs(BlockType blockType)
    {
        if(blockType == BlockType.Grass)
        {
            _uvs.Add(new Vector2(0.1f, 0.6f));
            _uvs.Add(new Vector2(0.4f, 0.6f));
            _uvs.Add(new Vector2(0.1f, 0.9f));
            _uvs.Add(new Vector2(0.4f, 0.9f));
        }
        else if (blockType == BlockType.Water)
        {
            _uvs.Add(new Vector2(0.6f, 0.6f));
            _uvs.Add(new Vector2(0.9f, 0.6f));
            _uvs.Add(new Vector2(0.6f, 0.9f));
            _uvs.Add(new Vector2(0.9f, 0.9f));
        }
        else if (blockType == BlockType.Barer)
        {
            _uvs.Add(new Vector2(0.1f, 0.1f));
            _uvs.Add(new Vector2(0.4f, 0.1f));
            _uvs.Add(new Vector2(0.1f, 0.4f));
            _uvs.Add(new Vector2(0.4f, 0.4f));
        }
        else if(blockType == BlockType.Brige)
        {
            _uvs.Add(new Vector2(0.6f, 0.1f));
            _uvs.Add(new Vector2(0.9f, 0.1f));
            _uvs.Add(new Vector2(0.6f, 0.4f));
            _uvs.Add(new Vector2(0.9f, 0.4f));
        }
    }
}
