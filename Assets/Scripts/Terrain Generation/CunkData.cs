﻿using UnityEngine;

public class CunkData
{
    public Vector2Int ChunkPosition;
    public Renderer Renderer;
    public BlockType[,,] Blocks;
}

