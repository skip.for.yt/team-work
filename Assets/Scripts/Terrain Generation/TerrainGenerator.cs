using System;
using UnityEngine;
using UnityEngine.Serialization;

public class TerrainGenerator : MonoBehaviour
{
    public float _baseHeight = 4;
    public NoiseOctaveSettings[] _octaves;
    public NoiseOctaveSettings _domainWarp;
    public RiverGenerator _riverGenerator;
    public BrigeGenerator _brickGenerator;

    [Serializable]
    public class NoiseOctaveSettings
    {
        public FastNoiseLite.NoiseType _noiseType;
        public float _frequency = 0.2f;
        public float _amplitude = 1.0f;
    }

    private FastNoiseLite[] _octaveNoises;
    private FastNoiseLite _warpNoise;

    private void Awake() => Init();
    
    public void Init()
    {
        _octaveNoises = new FastNoiseLite[_octaves.Length];
        for (int i = 0; i < _octaveNoises.Length; i++)
        {
            _octaveNoises[i] = new FastNoiseLite();
            _octaveNoises[i].SetNoiseType(_octaves[i]._noiseType);
            _octaveNoises[i].SetFrequency(_octaves[i]._frequency);
        }

        _warpNoise = new FastNoiseLite();
        _warpNoise.SetNoiseType(_domainWarp._noiseType);
        _warpNoise.SetFrequency(_domainWarp._frequency);
        _warpNoise.SetDomainWarpAmp(_domainWarp._amplitude);
        _riverGenerator.Init();
        _brickGenerator.Init();
    }

    public BlockType[,,] GenerateTerrain(float xOffset, float zOffset)
    {

        var result = new BlockType[ChunkRenderer.ChunkWight, ChunkRenderer.ChunkHeight, ChunkRenderer.ChunkWight];

        for (int x = 0; x < ChunkRenderer.ChunkWight; x++)
        {
            for(int z = 0; z < ChunkRenderer.ChunkWight; z++)
            {
                //float height = Mathf.PerlinNoise((x/2 + xOffset)/ 25f, (z/2 + zOffset)/ 25f) * 5;


                if (_riverGenerator.BlockIsRiver(x * ChunkRenderer.BlockScale + xOffset, z * ChunkRenderer.BlockScale + zOffset))
                {
                    /*if (_brickGenerator.BlockIsBrige(x * ChunkRenderer.BlockScale + xOffset, z * ChunkRenderer.BlockScale + zOffset))
                    {
                        result[x, (int)_baseHeight, z] = BlockType.Brige;
                    }
                    else
                    {*/
                        int y = 0;  
                        for (; y < _baseHeight; y++)
                        {
                            result[x, y, z] = BlockType.Water;
                        }
                        y++;
                        for (; y < _baseHeight + 5; y++)
                        {
                            result[x, y, z] = BlockType.Barer;
                        }
                    //}
                }
                else
                {
                    float height = GetHeight(x * ChunkRenderer.BlockScale + xOffset, z * ChunkRenderer.BlockScale + zOffset);

                    for (int y = 0; y < height; y++)
                    {
                        result[x, y, z] = BlockType.Grass;
                    }
                }
            }
        }
        
        return result;
    }

    private float GetHeight(float x, float y)
    {
        _warpNoise.DomainWarp(ref x, ref y);
        
        float result = _baseHeight;


        for(int i = 0; i < _octaves.Length; i++)
        {
            float noise = _octaveNoises[i].GetNoise(x, y);
            result += noise * _octaves[i]._amplitude / 2;
        }

        return result; 
    }
}
