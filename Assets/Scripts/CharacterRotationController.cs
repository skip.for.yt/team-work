using System;
using UnityEngine;
using UnityEngine.Serialization;

public class CharacterRotationController : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed = 10f;

    private void Update()
    {
        var direction = MousePosition.GetPosition() - transform.position;
        direction.y = 0;
        var targetRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, _rotationSpeed * Time.deltaTime);
    }
}