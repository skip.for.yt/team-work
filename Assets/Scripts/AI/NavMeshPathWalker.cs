using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;

public class NavMeshPathWalker : MonoBehaviour
{
    [SerializeField] private float _speed = 3f;
    [SerializeField] private float _turnSpeed = 10f;

    private NavMeshAgent _navMeshAgent;
    private NavMeshPath _navMeshPath;
    private Transform _player;
    private int _currentWaypointIndex;
    
    private void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshPath = new NavMeshPath();
        _player = CharacterMovementController.GetTransform();
    }

    private void Update()
    {
        SetDestination(_player.position);
        if (_navMeshAgent.hasPath)
        {
            Vector3 targetPosition = _navMeshAgent.path.corners[_currentWaypointIndex];
            Vector3 direction = (targetPosition - transform.position).normalized;
            direction.y = 0;
            Quaternion targetRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, _turnSpeed * Time.deltaTime);
            
            if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
            {
                _currentWaypointIndex++;
                if (_currentWaypointIndex >= _navMeshAgent.path.corners.Length)
                {
                    _navMeshAgent.ResetPath();
                }
            }
        }
    }

    public void SetDestination(Vector3 destination)
    {
        _navMeshAgent.CalculatePath(destination, _navMeshPath);
        _currentWaypointIndex = 1;
        _navMeshAgent.SetPath(_navMeshPath);
    }
}