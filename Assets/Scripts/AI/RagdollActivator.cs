using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RagdollActivator : MonoBehaviour {
    private Animator _animator;
    private NavMeshAgent _navMeshAgent;
    private Collider _collider;
    [SerializeField] private Rigidbody[] _rigidbodies;

    private void Start() {
        _animator = GetComponent<Animator>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _collider = GetComponent<CapsuleCollider>();
    }

    public void ActivateRagdoll() {
        _animator.enabled = false;
        _navMeshAgent.enabled = false;
        _collider.enabled = false;
        
        foreach (Rigidbody rigidbody in _rigidbodies)
            rigidbody.isKinematic = false;
    }

    public void DeactivateRagdoll() {
        foreach (Rigidbody rigidbody in _rigidbodies)
            rigidbody.isKinematic = true;

        _animator.enabled = true;
        _navMeshAgent.enabled = true;
        _collider.enabled = true;
    }
}