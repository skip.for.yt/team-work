using System.Threading.Tasks;
using UnityEngine;

public class CharacterMovementController : MonoBehaviour
{
    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _rotateSpeed;

    private static CharacterMovementController _instance;
    private CharacterController _character;


    private void Awake()
    {
        _instance = this;
        _character = GetComponent<CharacterController>();
    }

    public static Transform GetTransform() => _instance.transform;

    private void Update()
    {
        var direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical")).normalized;
        _character.SimpleMove(direction * _moveSpeed);
    }
}
