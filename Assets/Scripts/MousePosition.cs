using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePosition : MonoBehaviour
{
    private Vector3 _mousePosition;
    private Camera _cam;
    private static MousePosition _instance;

    void Awake()
    {
        _instance = this;
        _cam = Camera.main;
    }

    public static ref Vector3 GetPosition() => ref _instance._mousePosition;

    void Update()
    {
        RaycastHit hit;
        Ray ray = _cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 7))
            _mousePosition = hit.point;
    }
}
