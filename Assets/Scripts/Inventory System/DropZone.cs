using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropZone : MonoBehaviour
{
    private List<Transform> _drops = new List<Transform>();
    private Transform _lastDrop;

    private void Update()
    {
        var minDistance = float.MaxValue;
        Transform newDrop = null;
        foreach (var drop in _drops)
        {
            var distance = Vector3.Distance(transform.position, drop.position);
            if (distance < minDistance)
            {
                minDistance = distance;
                newDrop = drop;
            }
        }

        if (_lastDrop != newDrop)
        {
            if (_lastDrop)
            {
                _lastDrop.GetComponent<Outline>().enabled = false;
                _lastDrop = null;
            }

            if (newDrop && newDrop.CompareTag("Drop"))
            {
                _lastDrop = newDrop;
                _lastDrop.GetComponent<Outline>().enabled = true;   
            }
        }

        if (Input.GetKeyUp(KeyCode.F))
        {
            var basicItem = newDrop.GetComponent<BasicItem>();
            if (newDrop && newDrop.CompareTag("Drop") && InventorySystem.AddItem(basicItem))
            {
                RemoveDrop(newDrop);
                InventorySystem.PickupDrop(basicItem);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Drop") || other.transform.CompareTag("WaitingDrop"))
            _drops.Add(other.transform);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("Drop") || other.transform.CompareTag("WaitingDrop"))
            RemoveDrop(other.transform);
    }

    public void RemoveDrop(Transform drop)
    {
        drop.GetComponent<Outline>().enabled = false;
        _drops.Remove(drop);
    }
}
