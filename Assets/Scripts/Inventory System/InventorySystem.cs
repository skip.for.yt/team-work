using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventorySystem : MonoBehaviour
{
    [SerializeField] private Cell[] _cells = new Cell[10];
    [SerializeField] private Color _unusedBackgroundColor;
    [SerializeField] private Color _usedBackgroundColor;
    [SerializeField] private float _droppingForce;
    [SerializeField] private Transform _itemsSpawn;
    [SerializeField] private GameObject _hand;

    private BasicItem _lastItem;
    private static InventorySystem _instance;
    private int _nowCell;

    private void Awake()
    {
        UpdateInventoryUI();
        _instance = this;
    }


    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Q) && _cells[_nowCell].ItemInfo)
        {
            var drop = DeleteItem(_cells[_nowCell]);
            drop.transform.SetParent(null);
            drop.SetActive(true);
            drop.UpdateState(BasicItem.ItemState.Drop);
            drop.GetComponent<Rigidbody>().AddForce(_itemsSpawn.forward * _droppingForce);
        }

        if (Input.GetKeyUp(KeyCode.Alpha1))
            OnClickCell(0);
        else if (Input.GetKeyUp(KeyCode.Alpha2))
            OnClickCell(1);
        else if (Input.GetKeyUp(KeyCode.Alpha3))
            OnClickCell(2);
        else if (Input.GetKeyUp(KeyCode.Alpha4))
            OnClickCell(3);
        else if (Input.GetKeyUp(KeyCode.Alpha5))
            OnClickCell(4);
    }
    
    public void OnClickCell(int id)
    {
        var lastCell = _nowCell;
        _nowCell = id;
        UpdateCell(_cells[lastCell]);
        UpdateCell(_cells[_nowCell]);
        UpdateItem(_cells[_nowCell]);
    }

    private void UpdateInventoryUI()
    {
        foreach (var cell in _cells)
            UpdateCell(cell);
    }

    private void UpdateCell(Cell cell)
    {
        cell._background.color = _cells[_nowCell] == cell ? _usedBackgroundColor : _unusedBackgroundColor;

        if (cell.ItemInfo)
        {
            cell._icon.color = Color.white;
            cell._icon.sprite = cell.ItemInfo._icon;
            cell._countText.text = cell.Objects.Count.ToString();
        }
        else
        {
            cell._icon.color = Color.clear;
            cell._countText.text = String.Empty;
        }
        
        if (cell == _cells[_nowCell])
            UpdateItem(_cells[_nowCell]);
    }

    private void UpdateItem(Cell cell)
    {
        _lastItem?.SetActive(false);
        BasicItem item;
        if (cell.Objects.TryPeek(out item))
        {
            _lastItem = item;
            _lastItem.SetActive(true);
        }
    }

    public static void PickupDrop(BasicItem basicItem) =>  _instance.StartCoroutine(basicItem.OnPickupZoneEntered(_instance._itemsSpawn));

    public static bool AddItem(BasicItem basicItem)
    {
        foreach (var cell in _instance._cells)
            if (cell.ItemInfo == null || (cell.ItemInfo == basicItem._itemInfo && cell.Objects.Count + 1 <= basicItem._itemInfo._maxCountInCell))
            {
                cell.ItemInfo = basicItem._itemInfo;
                cell.Objects.Push(basicItem);
                _instance.UpdateCell(cell);
                return true;
            }

        return false;
    }

    public BasicItem DeleteItem(Cell cell)
    {
        var obj = cell.Objects.Pop();
        if (cell.Objects.Count <= 0)
            cell.ItemInfo = null;

        _instance.UpdateCell(cell);
        return obj;
    }
}

[Serializable]
public class Cell
{
    [NonSerialized] public Stack<BasicItem> Objects = new Stack<BasicItem>();
    [NonSerialized] public ItemInfo ItemInfo;
    public Image _icon;
    public Image _background;
    public TextMeshProUGUI _countText;
}