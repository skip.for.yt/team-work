using System;
using System.Collections;
using UnityEngine;

public abstract class BasicItem : MonoBehaviour
{
    public enum ItemState
    {
        Drop,
        Item
    }

    public ItemInfo _itemInfo;
    [SerializeField] private  Vector3 _positionOffset;
    [SerializeField] private Vector3 _rotationOffset;
    
    protected ItemState State;
    private MeshCollider _mesh;
    private Rigidbody _rigidbody;
    private bool _isUsed;
    
    private void Awake()
    {
        _mesh = GetComponent<MeshCollider>();
        _rigidbody = GetComponent<Rigidbody>();
    }
    
    public virtual void UpdateState(ItemState state)
    {
        State = state;
        if (State == ItemState.Drop)
        {
            _isUsed = false;
            _mesh.enabled = true;
            _rigidbody.isKinematic = false;
            gameObject.tag = "Drop";
        }
    }

    public void SetActive(bool value)
    {
        _isUsed = value;
        if (State != ItemState.Drop)
            gameObject.SetActive(value);
    }
    
    public IEnumerator OnPickupZoneEntered(Transform goal)
    {
        if (State == ItemState.Drop)
        {
            gameObject.tag = "Player";
            _mesh.enabled = false;
            _rigidbody.isKinematic = true;
            transform.SetParent(goal);
            
            var pos = transform.localPosition;
            var rot = transform.localRotation;
            var scale = transform.localScale;
            for (float i = 0; i < 1.1; i+=0.1f)
            {
                transform.localPosition = Vector3.Lerp(pos, _positionOffset, i);
                transform.localRotation = Quaternion.Lerp(rot, Quaternion.Euler(_rotationOffset), i);
                transform.localScale = Vector3.Lerp(scale, Vector3.zero, i);
                yield return new WaitForSeconds(0.01f);
            }

            transform.localScale = scale;
            State = ItemState.Item;
            if (!_isUsed)
                gameObject.SetActive(false);
        }
    }
}
