using System.Collections;
using UnityEngine;

public class Gun : BasicItem
{
    [SerializeField] private int _ammoCapacity = 10; // емкость магазина
    [SerializeField] private float _fireRate = 0.5f; // скорострельность
    [SerializeField] private float _reloadTime = 2f; // время перезарядки
    [SerializeField] private float _damage = 10f; // урон
    [SerializeField] private float _range = 100f; // дальность стрельбы
    [SerializeField] private Transform _firePoint; // точка выстрела
    [SerializeField] private GameObject _fxBlood;

    private int _ammoCount; // количество патронов в магазине
    private bool _isReloading; // флаг перезарядки
    private float _nextFireTime; // время следующего выстрела

    private void Start()
    {
        _ammoCount = _ammoCapacity;
        _isReloading = false;
        _nextFireTime = 0f;
    }

    private void Update()
    {
        if (State == ItemState.Item)
        {
            if (Input.GetMouseButton(0) && Time.time >= _nextFireTime && !_isReloading)
            {
                Shoot();
                _nextFireTime = Time.time + _fireRate;
            }

            if (Input.GetKeyUp(KeyCode.R) && _ammoCount < _ammoCapacity  && !_isReloading)
                StartCoroutine(Reload());
        }
    }

    private void Shoot()
    {
        if (_ammoCount <= 0)
        {
            StartCoroutine(Reload());
            return;
        }

        RaycastHit hit;
        if (Physics.Raycast(_firePoint.position, _firePoint.forward, out hit, _range))
        {
            var enemy = hit.transform.GetComponent<Zombie>();
            if (enemy)
            {
                enemy.OnTakeDamage(_damage);
                Instantiate(_fxBlood, hit.point, Quaternion.LookRotation(-_firePoint.forward, Vector3.up), enemy.transform);
            }
        }

        _ammoCount--;
    }

    private IEnumerator Reload()
    {
        _isReloading = true;
        
        yield return new WaitForSeconds(_reloadTime);

        _ammoCount = _ammoCapacity;
        _isReloading = false;
    }
}