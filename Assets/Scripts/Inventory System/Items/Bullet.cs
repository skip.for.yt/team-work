using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [NonSerialized] public float Damage;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Zombie"))
            other.GetComponent<Zombie>().OnTakeDamage(Damage);
        
        Destroy(gameObject);
    }
}
