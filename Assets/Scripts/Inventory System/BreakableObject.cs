using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BreakableObject : MonoBehaviour
{
    [SerializeField] private AnimationClip _animationOnDamage;
    [SerializeField] private float _health;
    [SerializeField] private DropoutInformation[] _dropoutInformation;
    private Animation _animation;

    private void Awake() => _animation = GetComponent<Animation>();

    public void GetDamage(float damage)
    {
        _health -= damage;
        if (_health <= 0)
        {
            foreach (var dropoutInformation in _dropoutInformation)
            {
                var random = Random.Range(0, 100);
                if (dropoutInformation._chance <= random)
                {
                    random = Random.Range(1, dropoutInformation._count + 1);
                    for (int i = 0; i < random; i++)
                    {
                        // спавнить дроп
                    }
                }
            }
            
            Destroy(gameObject);
        }
        else
        {
            _animation.clip = _animationOnDamage;
            _animation.Play();
        }
    }
}

[Serializable]
public class DropoutInformation
{
    public GameObject _drop;
    [Range(0, 10)] public int _count;
    [Range(0, 101)] public int _chance;
}
