using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemInfo", menuName = "ItemInfo")]
public class ItemInfo : ScriptableObject
{
    public Sprite _icon;
    public GameObject _item;
    public GameObject _drop;
    public int _maxCountInCell;
}
