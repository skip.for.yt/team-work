public abstract class ScreenWindow : Window { }

public abstract class ScreenWindow<T> : Window<T> { }