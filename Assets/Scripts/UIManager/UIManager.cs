using System;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public List<Window> _windows = new ();
    private Dictionary<Type, Window> _windowDictionary = new ();
    private Window _openedPopUp;
    private Window _openedScreen;

    private void Awake()
    {
        foreach (var window in _windows)
        {
            _windowDictionary.Add(window.GetType(),window);
            window.UIManager = this;
        }
    }

    public void Open<T>() where T : Window
    {
        var window = Get<T>();
        if (window is PopUpWindow)
            UpdatePopUp(window);
        else
            UpdateScreen(window);
        
        window?.gameObject.SetActive(true);
        window?.OnOpen();
    }
    
    public void Open<TType, TData>(TData obj) where TType : Window
    {
        var window = Get<TType>() as Window<TData>;
        if (window is PopUpWindow<TData>)
            UpdatePopUp(window);
        else
            UpdateScreen(window);
        
        window?.gameObject.SetActive(true);
        window?.OnOpen(obj);
    }

    public void Close<T>() where T : Window
    {
        var window = Get<T>();
        if (window.gameObject.activeSelf)
        {
            window?.gameObject.SetActive(false);
            window?.OnClose();
        }
    }

    private void UpdatePopUp(Window window)
    {
        CloseOpenedPopUp();
        _openedPopUp = window;
    }

    private void CloseOpenedPopUp()
    {
        _openedPopUp?.gameObject.SetActive(false);
        _openedPopUp?.OnClose();
        _openedPopUp = null;
    }
    
    private void UpdateScreen(Window window)
    {
        _openedScreen?.gameObject.SetActive(false);
        _openedScreen?.OnClose();
        _openedScreen = window;
        CloseOpenedPopUp();
    }
    
    private T Get<T>() where T : Window
    {
        return (T)_windowDictionary[typeof(T)];
    }
}