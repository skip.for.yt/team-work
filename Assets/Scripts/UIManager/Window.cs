using System;
using UnityEngine;

public abstract class Window : MonoBehaviour
{
    [HideInInspector] public UIManager UIManager;
    
    public virtual void OnOpen(){}

    public virtual void OnClose(){}
}

public abstract class Window<T> : Window
{
    public virtual void OnOpen(T obj) {}
}